//Brian Kirkov
//2134488

package linearalgebra;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Vector3dTest {
    @Test
    public void testGetMethods() {
        Vector3d v = new Vector3d(10.0, 15.0, 20.0);
        // assertEquals("Making sure get methods return correctly", 1.0, v.getX(), 0.00000000000001); // --> fails
        // assertEquals("Making sure get methods return correctly", 1.0, v.getY(), 0.00000000000001); // --> fails
        // assertEquals("Making sure get methods return correctly", 2.0, v.getZ(), 0.00000000000001); // --> fails
        assertEquals("Making sure get methods return correctly", 10.0, v.getX(), 0.00000000000001);
        assertEquals("Making sure get methods return correctly", 15.0, v.getY(), 0.00000000000001);
        assertEquals("Making sure get methods return correctly", 20.0, v.getZ(), 0.00000000000001);
    }

    @Test
    public void testMagnitude() {
        Vector3d v = new Vector3d(10.0, 15.0, 20.0);
        // assertEquals("Making sure magnitude method returns correctly", 2.2, v.magnitude(), 0.00000000000001); // --> fails
        assertEquals("Making sure magnitude method returns correctly", 26.92582403567252, v.magnitude(), 0.00000000000001);
    }

    @Test
    public void testDotProduct() {
        Vector3d v = new Vector3d(10.0, 15.0, 20.0);
        Vector3d w = new Vector3d(5.0, 7.5, 12.5);
        // assertEquals("Making sure dotProduct method returns correctly", 4.5, v.dotProduct(w), 0.00000000000001); // --> fails
        assertEquals("Making sure dotProduct method returns correctly", 412.5, v.dotProduct(w), 0.00000000000001);
    }

    @Test
    public void testAdd() {
        Vector3d v = new Vector3d(10.0, 15.0, 20.0);
        Vector3d w = new Vector3d(5.0, 7.5, 12.5);
        // assertEquals("Making sure add method returns correctly", 1.5, v.add(w).getX(), 0.00000000000001); // --> fails
        // assertEquals("Making sure add method returns correctly", 2.5, v.add(w).getY(), 0.00000000000001); // --> fails
        // assertEquals("Making sure add method returns correctly", 3.5, v.add(w).getZ(), 0.00000000000001); // --> fails
        assertEquals("Making sure add method returns correctly", 15.0, v.add(w).getX(), 0.00000000000001);
        assertEquals("Making sure add method returns correctly", 22.5, v.add(w).getY(), 0.00000000000001);
        assertEquals("Making sure add method returns correctly", 32.5, v.add(w).getZ(), 0.00000000000001);
    }
}
