//Brian Kirkov
//2134488

package linearalgebra;

public class Vector3d {
  private double x;
  private double y;
  private double z;

  public Vector3d(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }

  public double getX() {
    return this.x;
  }

  public double getY() {
    return this.y;
  }

  public double getZ() {
    return this.z;
  }

  public double magnitude() {
    double m = Math.sqrt( (this.x*this.x) + (this.y*this.y) + (this.z*this.z) );
    return m;
  }

  public double dotProduct(Vector3d o) {
    double dp = ( (this.x*o.x) + (this.y*o.y) + (this.z*o.z) );
    return dp;
  }

  public Vector3d add(Vector3d o) {
    Vector3d n = new Vector3d((this.x+o.x), (this.y+o.y), (this.z+o.z));
    return n;
  }
}