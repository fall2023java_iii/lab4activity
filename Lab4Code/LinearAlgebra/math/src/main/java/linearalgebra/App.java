package linearalgebra;

public class App {
    public static void main(String[] args) {
        // Vector3d vOne = new Vector3d(4.0, 5.0, 6.0);
        // Vector3d vTwo = new Vector3d(1.0, 2.0, 3.0);
        
        // System.out.println(vOne.getX());
        // System.out.println(vOne.getY());
        // System.out.println(vOne.getZ());
        // System.out.println(vTwo.getX());
        // System.out.println(vTwo.getY());
        // System.out.println(vTwo.getZ());

        // System.out.println(vOne.magnitude());
        // System.out.println(vTwo.magnitude());
        // System.out.println(vOne.dotProduct(vTwo));
        // Vector3d newV = vOne.add(vTwo);
        // System.out.println(newV.getX());
        // System.out.println(newV.getY());
        // System.out.println(newV.getZ());

        Vector3d vTest = new Vector3d(10.0, 15.0, 20.0);
        System.out.println(vTest.magnitude());
        
        Vector3d wTest = new Vector3d(5.0, 7.5, 12.5);
        System.out.println(vTest.dotProduct(wTest));

        System.out.println(vTest.add(wTest).getX());
        System.out.println(vTest.add(wTest).getY());
        System.out.println(vTest.add(wTest).getZ());
    }
}
